
import android.test.ActivityTestCase;

import com.mobapply.ordersapp.models.LocationAnswer;
import com.mobapply.ordersapp.models.Order;
import com.mobapply.ordersapp.utils.GeocodeApiUtil;
import com.mobapply.ordersapp.utils.ServerApiUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@RunWith(JUnit4.class)
public class ServerApiTest extends ActivityTestCase {

    ServerApiUtil util;

    @Before
    public void init(){

        util = ServerApiUtil.init();
    }

    @Test
    public void testOrdersSize(){

        util.getOrders(new Callback<List<Order>>() {

            @Override
            public void success(List<Order> orders, Response response) {

                assertEquals(16, orders.size());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Test
    public void testMapping(){

        util.getOrders(new Callback<List<Order>>() {

            @Override
            public void success(List<Order> orders, Response response) {

                if(orders.size() > 0) {
                    String city = orders.get(0).getDepatureAddress().getCity();
                    assertEquals("Berlin", city);
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


}