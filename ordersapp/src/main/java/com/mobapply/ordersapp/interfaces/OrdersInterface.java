package com.mobapply.ordersapp.interfaces;

import com.google.android.gms.maps.model.LatLng;
import com.mobapply.ordersapp.models.Order;
import com.mobapply.ordersapp.models.PolyLine;

import java.util.List;

/**
 * Created by tised on 7/29/15.
 */
public interface OrdersInterface {

    void setOrdersList(List<Order> orders);
    void addPolyLine(PolyLine polyline);
    void addMarker(LatLng position, float color);

}
