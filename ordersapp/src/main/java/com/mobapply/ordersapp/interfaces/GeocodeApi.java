package com.mobapply.ordersapp.interfaces;

import com.mobapply.ordersapp.models.GeocodeResponse;

import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Created by tised on 7/28/15.
 */
public interface GeocodeApi {

//    @GET("/geocode/json")
//    void getLocationData(@Query("address") String address, @Query("key") String key,
//                         Callback<LocationAnswer> results);

    @GET("/geocode/json")
    GeocodeResponse getLocationData(@Query("address") String address,
                                    @Query("components") String components, @Query("key") String key);


}
