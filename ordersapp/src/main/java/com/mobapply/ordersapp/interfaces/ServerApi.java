package com.mobapply.ordersapp.interfaces;

import com.mobapply.ordersapp.models.Order;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by tised on 7/28/15.
 */

public interface ServerApi {

    @GET("/orders/")
    void getOrders(Callback<List<Order>> callback);
}
