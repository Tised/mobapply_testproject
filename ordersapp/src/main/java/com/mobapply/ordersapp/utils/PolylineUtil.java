package com.mobapply.ordersapp.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.mobapply.ordersapp.R;
import com.mobapply.ordersapp.interfaces.OrdersInterface;
import com.mobapply.ordersapp.models.GeocodeResponse;
import com.mobapply.ordersapp.models.Order;
import com.mobapply.ordersapp.models.OrderAddress;
import com.mobapply.ordersapp.models.PolyLine;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tised on 7/29/15.
 */
public class PolylineUtil {

    private OrdersInterface ordersInterface;
    private static String  LOCATION = "LOCATION";
    private Context context;
    private GeocodeApiUtil geocodeApiUtil;
    private Map<String, Locale> localeMap;

    Handler mainHandler;

    public PolylineUtil(OrdersInterface ordersInterface, Context context){

        this.ordersInterface = ordersInterface;
        this.context = context;
        geocodeApiUtil = GeocodeApiUtil.init();
        initCountryCodeMapping();
    }

    public void generatePolylines(List<Order> orders){

        mainHandler = new Handler(context.getMainLooper());

        ExecutorService positionsExecutor = Executors.newFixedThreadPool(4);

        for (final Order order : orders){

            final PolyLine polyLine = new PolyLine();

            positionsExecutor.execute(new Runnable() {
                @Override
                public void run() {

                    GeocodeResponse departureResponse = geocodeApiUtil.getLocationData(generateAddress(order.getDepatureAddress()),
                            generateComponents(order.getDepatureAddress()), context.getString(R.string.google_geocoding_key));

                    GeocodeResponse destinationResponse = geocodeApiUtil.getLocationData(generateAddress(order.getDestinationAddress()),
                            generateComponents(order.getDestinationAddress()), context.getString(R.string.google_geocoding_key));

                    if (departureResponse.getResults().size() > 0 && departureResponse.getStatus().equals("OK") &&
                            destinationResponse.getResults().size() > 0 && destinationResponse.getStatus().equals("OK")) {

                        final LatLng departurePosition = departureResponse.getResults().get(0).getGeometry().getLocation().getLatLng();
                        final LatLng destinationPosition = destinationResponse.getResults().get(0).getGeometry().getLocation().getLatLng();

                        polyLine.setDepartureLatLng(departurePosition);
                        polyLine.setDestinationLatLng(destinationPosition);

                        Log.d(LOCATION, "founded departure lat ");

                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                ordersInterface.addMarker(departurePosition, BitmapDescriptorFactory.HUE_GREEN);
                                ordersInterface.addMarker(destinationPosition, BitmapDescriptorFactory.HUE_RED);
                                ordersInterface.addPolyLine(polyLine);
                            }
                        });
                    }
                }
            });

        }

        positionsExecutor.shutdown();
        while (!positionsExecutor.isTerminated()) {
        }
    }

    private String generateComponents(OrderAddress orderAddress){

        return "country:" + iso3ToIso2(orderAddress.getCountryCode()) + "|" + "locality:" + orderAddress.getCity() + "|" + "postal_code:" +orderAddress.getZipCode();
    }

    private String generateAddress(OrderAddress order){

        String address = "";

        if(!order.getHouseNumber().equals(""))
            address += order.getHouseNumber() + ", ";
        if(!order.getStreet().equals(""))
            address += order.getStreet() + ", ";

        Log.i(LOCATION, "address to find === " + address);

        return address;
    }

    private String iso3ToIso2(String iso3CountryCode) {

        return localeMap.get(iso3CountryCode).getCountry();
    }

    private void initCountryCodeMapping() {

        String[] countries = Locale.getISOCountries();
        localeMap = new HashMap<String, Locale>(countries.length);
        for (String country : countries) {
            Locale locale = new Locale("", country);
            localeMap.put(locale.getISO3Country().toUpperCase(), locale);
        }
    }
}
