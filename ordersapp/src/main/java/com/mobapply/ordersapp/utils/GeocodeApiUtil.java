package com.mobapply.ordersapp.utils;

import com.mobapply.ordersapp.interfaces.GeocodeApi;
import com.mobapply.ordersapp.models.GeocodeResponse;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by tised on 7/28/15.
 */
public class GeocodeApiUtil {

    private static GeocodeApiUtil apiInstance;
    GeocodeApi geocodeApi;

    public static GeocodeApiUtil init() {

        if (apiInstance ==null)
            apiInstance = new GeocodeApiUtil();

        return apiInstance;
    }

    private GeocodeApiUtil() {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(2, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(2, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://maps.googleapis.com/maps/api/")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .build();

        geocodeApi = restAdapter.create(GeocodeApi.class);
    }

    public GeocodeResponse getLocationData(String address, String components, String key){

        return geocodeApi.getLocationData(address, components, key);
    }

}
