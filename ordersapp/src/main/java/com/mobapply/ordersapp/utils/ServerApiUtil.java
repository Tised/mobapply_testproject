package com.mobapply.ordersapp.utils;

import com.mobapply.ordersapp.interfaces.ServerApi;
import com.mobapply.ordersapp.models.Order;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;

/**
 * Created by tised on 7/28/15.
 */
public class ServerApiUtil {

    private static ServerApiUtil apiInstance;
    ServerApi serverApi;

    public static ServerApiUtil init() {

        if (apiInstance ==null)
            apiInstance = new ServerApiUtil();

        return apiInstance;
    }

    private ServerApiUtil() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://mobapply.com/tests/")
                .build();

        serverApi = restAdapter.create(ServerApi.class);
    }

    public void getOrders(Callback<List<Order>> callback){

        serverApi.getOrders(callback);
    }

}
