package com.mobapply.ordersapp;

import android.app.Application;
import android.util.Log;

import com.mobapply.ordersapp.utils.ServerApiUtil;

/**
 * Created by tised on 7/28/15.
 */
public class OrdersApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();

        Log.d("ordersapp", "APP STARTED");

        ServerApiUtil.init();
    }
}
