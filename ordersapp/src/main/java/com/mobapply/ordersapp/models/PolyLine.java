package com.mobapply.ordersapp.models;


import com.google.android.gms.maps.model.LatLng;

/**
 * Created by tised on 7/29/15.
 */
public class PolyLine {

    LatLng departureLatLng;
    LatLng destinationLatLng;


    public LatLng getDestinationLatLng() {
        return destinationLatLng;
    }

    public void setDestinationLatLng(LatLng destinationLatLng) {
        this.destinationLatLng = destinationLatLng;
    }

    public LatLng getDepartureLatLng() {
        return departureLatLng;
    }


    public void setDepartureLatLng(LatLng latLng) {

        this.departureLatLng = latLng;
    }
}
