package com.mobapply.ordersapp.models;

/**
 * Created by tised on 7/28/15.
 */
public class Order {

    String uuid;
    String number;
    String good;
    int actualWeight;
    String appointmentFrom;
    String note1;
    int initialPrice;
    String status;

    DepartureAddress departureAddress;
    DestinationAddress destinationAddress;

    public DestinationAddress getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(DestinationAddress destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public DepartureAddress getDepatureAddress() {
        return departureAddress;
    }

    public void setDepatureAddress(DepartureAddress departureAddress) {
        this.departureAddress = departureAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(int initialPrice) {
        this.initialPrice = initialPrice;
    }

    public String getNote1() {
        return note1;
    }

    public void setNote1(String note1) {
        this.note1 = note1;
    }

    public String getAppointmentFrom() {
        return appointmentFrom;
    }

    public void setAppointmentFrom(String appointmentFrom) {
        this.appointmentFrom = appointmentFrom;
    }

    public int getActualWeight() {
        return actualWeight;
    }

    public void setActualWeight(int actualWeight) {
        this.actualWeight = actualWeight;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
