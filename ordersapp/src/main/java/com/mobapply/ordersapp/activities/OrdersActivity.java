package com.mobapply.ordersapp.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mobapply.ordersapp.R;
import com.mobapply.ordersapp.callbacks.OrdersCallback;
import com.mobapply.ordersapp.interfaces.OrdersInterface;
import com.mobapply.ordersapp.models.Order;
import com.mobapply.ordersapp.models.PolyLine;
import com.mobapply.ordersapp.utils.PolylineUtil;
import com.mobapply.ordersapp.utils.ServerApiUtil;

import java.util.List;

public class OrdersActivity extends FragmentActivity implements OrdersInterface {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ServerApiUtil mobApplyUtil;
    private PolylineUtil polylineUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        mobApplyUtil = ServerApiUtil.init();
        polylineUtil = new PolylineUtil(this, this);

        setUpMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
     //   setUpMap();
    }

    private void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                cameraInitPosition();
            }
        }

        mobApplyUtil.getOrders(new OrdersCallback(this));
    }

    private void cameraInitPosition() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51, 9), 5.0f));
    }

    @Override
    public void setOrdersList(List<Order> orders) {

        polylineUtil.generatePolylines(orders);
    }

    @Override
    public void addPolyLine(PolyLine polyline) {

        //possible marker without 2nd point, cuz of google geocoding response
        if(polyline.getDepartureLatLng() != null && polyline.getDestinationLatLng() !=null) {
            mMap.addPolyline(new PolylineOptions()
                            .add(polyline.getDepartureLatLng())
                            .add(polyline.getDestinationLatLng())
            );
        }
    }

    @Override
    public void addMarker(LatLng pos, float color) {

        mMap.addMarker(new MarkerOptions()
                .position(pos)
                .icon(BitmapDescriptorFactory
                        .defaultMarker(color)));
    }


}
