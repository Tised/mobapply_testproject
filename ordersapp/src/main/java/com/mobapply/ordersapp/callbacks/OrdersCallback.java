package com.mobapply.ordersapp.callbacks;

import android.util.Log;

import com.mobapply.ordersapp.interfaces.OrdersInterface;
import com.mobapply.ordersapp.models.Order;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by tised on 7/29/15.
 */
public class OrdersCallback implements Callback<List<Order>> {

    OrdersInterface ordersInterface;
    private static String ORDERS_LOADING_TAG = "GET ORDERS";

    public OrdersCallback(OrdersInterface ordersInterface){

        this.ordersInterface = ordersInterface;
    }

    @Override
    public void success(List<Order> ordersList, Response response) {

        Log.d(ORDERS_LOADING_TAG, "successful loaded " + ordersList.size());

        ordersInterface.setOrdersList(ordersList);
    }

    @Override
    public void failure(RetrofitError error) {

        Log.d(ORDERS_LOADING_TAG, "error while loading orders, message === " + error.getMessage());
    }
}
